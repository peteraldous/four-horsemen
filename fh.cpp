#include <iostream>

#include "four_horsemen.h"

using namespace std;

Storage oneToFive() {
    Storage s;
    s.add(1);
    s.add(2);
    s.add(3);
    s.add(4);
    s.add(5);
    return s;
}

int first(Storage s) {
    return s.get(0U);
}

int main() {
    Storage s;
    s = oneToFive();
    s.update(0, 100);
    int result = first(s);
    cout << "s:    " << s << endl;
    cout << "result: " << result << endl;
    return 0;
}
