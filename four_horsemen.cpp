#include "four_horsemen.h"

Storage::Storage() {
    _storage = new int[10U];
    _capacity = 10U;
    _size = 0U;
}

Storage::Storage(unsigned int capacity) {
    _storage = new int[capacity];
    _capacity = capacity;
    _size = 0U;
}

Storage::Storage(const Storage &other) {
    cout << "Copy" << endl;
    _capacity = other._capacity;
    _size = other._size;
    _storage = new int[_capacity];
    for(unsigned int i=0U; i<_size; ++i) {
        _storage[i] = other._storage[i];
    }
}

Storage::~Storage() {
    delete [] _storage;
}

Storage &Storage::operator=(const Storage &other) {
    cout << "Assignment" << endl;
    delete [] _storage;
    _capacity = other._capacity;
    _size = other._size;
    _storage = new int[_capacity];
    for(unsigned int i=0U; i<_size; ++i) {
        _storage[i] = other._storage[i];
    }
    return *this;
}

void Storage::add(int i) {
    if (_capacity == _size) {
        _capacity *= 2;
        int *new_storage = new int[_capacity];
        for (unsigned int i=0U; i<_size; ++i) {
            new_storage[i] = _storage[i];
        }
        delete [] _storage;
        _storage = new_storage;
    }
    _storage[_size++] = i;
}

int Storage::get(unsigned int index) {
    return _storage[index];
}

void Storage::update(unsigned int index, int i) {
    if (index >= 0U && index < _size) {
        _storage[index] = i;
    }
}

ostream &operator<<(ostream &os, const Storage &s) {
    os << "\tSize: " << s._size << endl;
    os << "\tCapacity: " << s._capacity << endl;
    os << "\t[";
    for (unsigned int i=0U; i<s._size; ++i) {
        os << s._storage[i];
        if (i < s._size - 1) {
            os << ", ";
        }
    }
    os << "]";
    return os;
}
