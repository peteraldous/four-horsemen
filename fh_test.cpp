#include <assert.h>
#include <iostream>
#include <string>

#include "four_horsemen.h"

const string NO_COLOR = "\033[0m";
const string RED = "\033[1;31m";
const string GREEN = "\033[1;32m";

// Testing that Storage holds onto its values
bool test_case_one() {
    Storage s;
    s.add(1);
    s.add(2);
    s.add(3);
    s.add(4);
    s.add(5);
    return s.get(0U) == 1 && s.get(1U) == 2 && s.get(2U) == 3 && s.get(3U) == 4
        && s.get(4U) == 5;
}

bool test_case_two() {
    bool passing = true;
    Storage s;
    s.add(1);
    passing = passing && s.get(0U) == 1;
    s.update(0U, 5);
    passing = passing && s.get(0U) == 5;
    s.update(0U, 100);
    passing = passing && s.get(0U) == 100;
    return passing;
}

int main() {
    unsigned int num_tests = 2;
    bool (*tests [num_tests])() = {test_case_one, test_case_two};
    for (unsigned int i=0; i<num_tests; ++i) {
        cout << "Test " << i+1 << " ";
        if (tests[i]()) {
            cout << GREEN << "passed";
        } else {
            cout << RED << "failed";
        }
        cout << NO_COLOR << endl;
    }
    return 0;
}
