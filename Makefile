# variables
GPP=g++
FLAGS=-g -w

# fh depends on fh.cpp and four_horsemen.o
# compile with $(GPP) using $(FLAGS); $^ is all prereqs
# in this case, $^ is "fh.cpp four_horsemen.o"
fh: fh.cpp four_horsemen.o
	$(GPP) $(FLAGS) $^ -o $@

%.o: %.cpp %.h
	$(GPP) $(FLAGS) -c -o $@ $<

fh_test: fh_test.cpp four_horsemen.o
	$(GPP) $(FLAGS) $^ -o $@

.PHONY: run
run: fh
	./fh

.PHONY: test
test: fh_test
	@./fh_test

.PHONY: clean
clean:
	rm -f *.o fh
