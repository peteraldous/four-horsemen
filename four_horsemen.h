#ifndef __FOUR_HORSEMEN_H
#define __FOUR_HORSEMEN_H

#include <iostream>

using namespace std;

class Storage {
    private:
        int *_storage;
        unsigned int _size;
        unsigned int _capacity;
    public:
        Storage();
        Storage(unsigned int);
        Storage(const Storage &);
        ~Storage();
        Storage &operator=(const Storage &);

        void add(int);
        int get(unsigned int);
        void update(unsigned int, int);

        friend ostream &operator<<(ostream &, const Storage &);
};

#endif // __FOUR_HORSEMEN_H
